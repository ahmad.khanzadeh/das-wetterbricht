import { useState } from "react";
import { AsyncPaginate } from "react-select-async-paginate";
import {GEO_API_URL, geoApiOptions} from '../api';

const Search=({onSearchChange})=>{
    const [search, setSearch]=useState(null);

    const handleOnChange=(searchData)=>{
            setSearch(searchData);
            onSearchChange(searchData);
    }

    const loadOptions=(inputValue)=>{
        return fetch(`${GEO_API_URL}/cities?minPopulation=100000&namePrefix=${inputValue}`, geoApiOptions)
        .then(response => response.json())
        .then(response => {
            return{
                options: response.data.map((city) =>{
                    return{
                        value: `${city.latitude} ${city.longitude}` ,
                        lable: `${city.name}, ${city.countryCode}`,
                    }
                })
             }
        })
        .catch(err => console.log(err)); 
    }
    return(
        <AsyncPaginate 
            placeholder='Suchen Sie Ihre Stadt!'
            debounceTimeout={6000}
            value={search}
            onChange={handleOnChange}
            loadOptions={loadOptions}
        />
    )
}

export default Search;